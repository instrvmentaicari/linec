cflags=-O2 -mtune=skylake
compiler=clang
linec: linec.c
	$(compiler) linec.c $(cflags) -o ./linec
install: linec
	sudo cp ./linec /usr/bin
uninstall:
	sudo rm /usr/bin/linec
asm:	linec.c
	$(compiler) ./linec.c $(cflags) -S -masm=intel -o ./linec.s
nasm:	linec.a
	nasm ./linec.a -o ./linec
	chmod 755 ./linec
testlinecspeed: testlinecspeed.a
	nasm ./testlinecspeed.a -o ./testlinecspeed
	chmod 755 ./testlinecspeed
clean:
	rm linec
